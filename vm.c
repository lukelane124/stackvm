#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "instructions.h"

void printStack(uint64_t* stackBase, uint64_t* stackTop)
{
	fprintf(stderr, "[DEBUGGING STACK DUMP]:\n");
	uint32_t stackSize = stackTop - stackBase;
	for (uint32_t i = 0; i < stackSize; i++)
	{
		fprintf(stderr, "%04d: 0x%016x\n", stackSize-1, stackTop[0-i]);
	}
}

uint8_t* run_machine(uint8_t* pc_initial, size_t maxBytesAvailable)
{
	// Program counter initialization.
	uint8_t* pc = pc_initial;
	
	// Scratch variable.
	uint64_t uv64;
	
	// Stack setup.
	uint64_t stack_initial[1024] = {0};
	uint64_t* stack = (uint64_t*) stack_initial;
	stack -= 1024;
	
	// Creation of Memory.
	uint64_t* memory = (uint64_t*) malloc(4096 * sizeof(uint64_t));
	
	bool cont = true;
	while(cont)
	{
		switch((instruction_t) *pc++)
		{
			case ADD:
			{
				*(stack-1) = (*stack) + (*(stack-1));
				stack--;
				break;
			}
			case SUB:
			{
				*(stack-1) = (*stack) - (*(stack-1));
				stack--;
				break;
			}
			case SHL:
			{
				*(stack-1) = (*stack) << (*(stack-1));
				stack--;
				break;
			}
			case SHR:
			{
				*(stack-1) = (*stack) << (*(stack-1));
				stack--;
				break;
			}
			case JMP:
			{
				memcpy(&uv64, pc, sizeof(uint64_t));
				pc = (pc_initial + uv64);
				break;
			}
			case PUSHI:
			{
				memcpy(&uv64, pc, sizeof(uint64_t));
				pc += sizeof(uint64_t);
				*++stack = uv64;
				break;
			}
			case EXIT:
			{
				fprintf(stdout, "return code: %u\n", *stack);
				cont = false;
			}
			case LTE:
			{
				uint64_t u1 = *stack, u2 = *(stack-1);
				stack -= 1;
				*stack = 0;
				if (u1 <= u2)
				{
						*stack = 1;
				}
				break;
			}
			case GT:
			{
				uint64_t u1 = *stack, u2 = *(stack-1);
				stack -= 1;
				*stack = 0;
				if (u1 > u2)
				{
						*stack = 1;
				}
				break;
			}
			case STORE:
			{
				memory[*stack-1] = *(stack);
				break;
			}
			case LOAD:
			{
				*(stack+1) = *stack;
				*stack = memory[*stack];
				stack++;
				break;
			}
			case NOT:
			{
				*stack = ~(*stack);
				break;
			}
			case POP:
			{
				stack--;
				break;
			}
			case INC:
			{
				(*stack)++;
				break;
			}
			case DEC:
			{
				(*stack)--;
				break;
			}
			case COPY:
			{
				*(stack+1) = *stack;
				stack++;
				break;
			}
			case SWAP:
			{
				(*stack) += (*(stack-1));
				(*(stack-1)) = (*(stack)) - (*(stack-1));
				(*stack) = (*stack) - (*(stack-1));
				break;
			}
			case DBG:
			{
				printStack((uint64_t*)stack_initial, stack);
				break;
			}
		}
	}
}

int main(int argc, char** argv, char** envp)
{
	int fdIn = open(argv[1], O_RDONLY);
	size_t fileOffset = 0;
	size_t fileSize;
	void* mapping = NULL;
	if (fdIn > 0)
	{
		struct stat statbuf;
		fstat(fdIn, &statbuf);
		fileSize = statbuf.st_size;
		mapping = mmap(NULL, fileSize, PROT_READ, MAP_SHARED, fdIn, fileOffset);
	}
	if (mapping != NULL)
	{
		uint8_t* pc = mapping;
		uint8_t* endingLocation = run_machine(pc, fileSize);
	}
}
