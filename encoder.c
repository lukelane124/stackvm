#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "instructions.h"

int main(int argc, char** argv, char** envp)
{
	uint64_t uv64;
	remove(argv[1]);
	int fdOut = open(argv[1], O_CREAT | O_RDWR | S_IRWXU);
	size_t fileOffset = 0;
	char answer[1024];
	uint8_t values[count] = {ADD,SUB,SHL,SHR,JMP,PUSHI,EXIT};
	if (fdOut > 0)
	{
		while(true)
		{
			fprintf(stdout, "Enter instruction: ");
			fgets(answer, sizeof(answer), stdin);
			if (!strncmp(answer, "ADD", 3))
			{
				write(fdOut, &values[ADD], 1);
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
			}
			else if (!strncmp(answer, "SUB", 3))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[SUB], 1);
			}
			else if (!strncmp(answer, "SHL", 3))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[SHL], 1);
			}
			else if (!strncmp(answer, "SHR", 3))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[SHR], 1);
			}
			else if (!strncmp(answer, "JMP", 3))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[JMP], 1);
			}
			else if (!strncmp(answer, "PUSHI", 5))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[PUSHI], 1);
			}
			else if (!strncmp(answer, "EXIT", 4))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[EXIT], 1);
				break;
			}
			else if (!strncmp(answer, "LTE", 3))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[LTE], 1);
			}
			else if (!strncmp(answer, "GT", 2))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[GT], 1);
			}
			else if (!strncmp(answer, "STORE", 5))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[STORE], 1);
			}
			else if (!strncmp(answer, "LOAD", 4))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[LOAD], 1);
			}
			else if (!strncmp(answer, "NOT", 3))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[NOT], 1);
			}
			else if (!strncmp(answer, "POP", 3))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[POP], 1);
			}
			else if (!strncmp(answer, "INC", 3))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[INC], 1);
			}
			else if (!strncmp(answer, "DEC", 3))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[DEC], 1);
			}
			else if (!strncmp(answer, "COPY", 4))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[COPY], 1);
			}
			else if (!strncmp(answer, "SWAP", 4))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[SWAP], 1);
			}
			else if (!strncmp(answer, "DBG", 3))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &values[DBG], 1);
			}
			else if (sscanf(answer, "%lu", &uv64))
			{
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				write(fdOut, &uv64, sizeof(uint64_t));
			}
			else
			{
				fprintf(stderr, "invalid input.\n");
				break;
				fprintf(stdout, "%d; you entered: %s\n", __LINE__, answer);
				fprintf(stdout, "Would you like to exit? [Y/n]: ");
				fgets(answer, sizeof(answer), stdin);
				fprintf(stdout, "%d; you entered: %s", __LINE__, answer);
				if (!strcasecmp(answer, "N"))
				{
					break;
				}

			}
			answer[0] = 0;
		}
	}
	close(fdOut);
	return 0;
}