# Expected Operations
# instruction organization
All instructions are one byte long. With an optional following set of bytes for immediate values.
All operations are carried out on 32 bit double words. If you need something
smaller clear the "upper" bits.
## Data relocation instructions.
### load
Load TOS from memory location
-> tos contains address
<- tos contains data from previous tos

### loadarray
Load array onto stack
-> location, length
<- len bytes added in reverse order to being encounered

### loadimmediate
Load immediate value to tos.
-> ...
<- immediate

### store
Store tos2 to tos
-> address, value

### storeimmediate
store tos to an immediate location.
-> value

## Math Operations
### Add
-> n1 n2
<- summation
All additions are 2's complement addition.

### And
bitwise and.
-> val, mask
<- result

### Or
bitwise Or.
-> val, mask
<- result

### Not
invert all bits.
-> n1, n2
<- result

## Control Flow Operations
### JMP
Always uses immediate value encoding.
Pushes the current pc to the callstack.
This will actually be the PC address plus 1 s.t. we don't keep calling the same
program when we return.

### RET
Load top of callstack to the PC.

