typedef enum
{
	ADD,
	SUB,
	SHL,
	SHR,
	JMP,
	PUSHI,
	EXIT,
	LTE,
	GT,
	STORE,
	LOAD,
	NOT,
	POP,
	INC,
	DEC,
	COPY,
	SWAP,
	DBG,
	count
} instruction_t;